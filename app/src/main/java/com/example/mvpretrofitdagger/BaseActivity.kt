package com.example.mvpretrofitdagger

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.droidnet.DroidListener
import com.droidnet.DroidNet
import com.example.mvpretrofitdagger.common.CommonUtils
import com.example.mvpretrofitdagger.common.SharedPref
import com.example.mvpretrofitdagger.rest.SBAppInterface
import com.google.gson.Gson
import retrofit2.Retrofit
import javax.inject.Inject


open class BaseActivity : AppCompatActivity(), DroidListener {

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    @Inject
    lateinit var sbAppInterface: SBAppInterface

    @Inject
    lateinit var gson: Gson
    @Inject
    lateinit var commonUtils: CommonUtils

    @Inject
    lateinit var  sharedPref: SharedPref

//    @Inject
//    lateinit var validation: Validation

    @Inject
    lateinit var retrofit: Retrofit

    var alertDialog:AlertDialog?=null
    private var fragmentTransaction: FragmentTransaction? = null
    private var fragmentManager: FragmentManager? = null

    private var mDroidNet: DroidNet? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as SBApp).getmNetComponent().inject(this)

        fragmentManager = supportFragmentManager
        fragmentTransaction = fragmentManager?.beginTransaction()


        fragmentManager?.addOnBackStackChangedListener {

            val length = fragmentManager?.backStackEntryCount
            println("Total stack length $length")

            for (index in 0 until length!!) {
                println(fragmentManager?.getBackStackEntryAt(index)?.name)
            }
        }

        mDroidNet = DroidNet.getInstance();
        mDroidNet!!.addInternetConnectivityListener(this);
    }

    override fun onResume() {
        super.onResume()
        SBApp.setActivityOpen(true)
    }

    override fun onPause() {
        super.onPause()
        SBApp.setActivityOpen(false)
    }

    override fun onDestroy() {
        super.onDestroy()
        mDroidNet!!.removeInternetConnectivityChangeListener(this)
    }


    fun setToolbarTitle(name: String) {
        getSupportActionBar()!!.setTitle(name)
    }

    override fun onBackPressed() {
       overridePendingTransition(0, 0)
            super.onBackPressed()
    }

    override fun onInternetConnectivityChanged(isConnected: Boolean) {

        if(isConnected)
        {
           //Toast.makeText(this,"Internet Connected activity",Toast.LENGTH_SHORT).show()
           hidedialog()

        }
        else
        {
           // Toast.makeText(this,"Internet Not Connected activity",Toast.LENGTH_SHORT).show()
            showdialog()
        }

    }

    private fun hidedialog() {

        if(alertDialog!=null)
        {
            alertDialog!!.dismiss()
       }

    }

    private fun showdialog()
    {
//        val builder = AlertDialog.Builder(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
//        builder.setCancelable(false)
//        // set the custom layout
//        // set the custom layout
//        val customLayout: View = layoutInflater.inflate(R.layout.dialog_no_internet, null)
//        builder.setView(customLayout)
//        alertDialog = builder.create()
//        alertDialog!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
//        val width = ViewGroup.LayoutParams.MATCH_PARENT
//        val height = ViewGroup.LayoutParams.MATCH_PARENT
//        alertDialog!!.getWindow()!!.setLayout(width, height)
//        alertDialog!!.show()
//        val btn_retry = alertDialog!!.findViewById<View>(R.id.btn_retry) as AppCompatButton?
//
//        btn_retry!!.setOnClickListener { alertDialog!!.dismiss() }

        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        // set the custom layout
        // set the custom layout
        val customLayout: View = layoutInflater.inflate(R.layout.dialog_no_internet, null)
        builder.setView(customLayout)

        alertDialog = builder.create()
        alertDialog!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        alertDialog!!.show()

        var btn_retry= alertDialog!!.findViewById<AppCompatButton>(R.id.btn_retry)

        btn_retry!!.setOnClickListener {
            if(isNetworkConnected())
            {
                 Toast.makeText(this,"Internet Connected",Toast.LENGTH_SHORT).show()
            }
            else
            {
                Toast.makeText(this,"Internet Connection not available",Toast.LENGTH_SHORT).show()
            }
        }
    }

     open fun isNetworkConnected(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null && cm.activeNetworkInfo.isConnected
    }

}