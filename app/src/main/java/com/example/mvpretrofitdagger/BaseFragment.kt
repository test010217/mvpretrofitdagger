package com.example.mvpretrofitdagger

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.droidnet.DroidListener
import com.droidnet.DroidNet
import com.example.mvpretrofitdagger.common.BaseUseCaseImpl_MembersInjector
import com.example.mvpretrofitdagger.common.CommonUtils
import com.example.mvpretrofitdagger.common.SharedPref
import com.example.mvpretrofitdagger.rest.SBAppInterface
import com.google.gson.Gson
import retrofit2.Retrofit
import javax.inject.Inject


open class BaseFragment : Fragment() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    @Inject
    lateinit var sbAppInterface: SBAppInterface

    @Inject
    lateinit var gson: Gson
    @Inject
    lateinit var commonUtils: CommonUtils

    @Inject
    lateinit var sharedPref: SharedPref

//    @Inject
//    lateinit var validation: Validation

    @Inject
    lateinit var retrofit: Retrofit

    private var rootView:View? =null

    private var mDroidNet: DroidNet? = null

    var alertDialog:AlertDialog?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as SBApp).getmNetComponent().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRootView(view)
     //   mDroidNet = DroidNet.getInstance();
   //     mDroidNet!!.addInternetConnectivityListener(this);
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
      //  mDroidNet!!.removeInternetConnectivityChangeListener(this)
    }

    fun getRootView(): View? {
        return rootView
    }

    fun setRootView(rootView: View?) {
        this.rootView = rootView
    }

    companion object
    {
        @JvmStatic
        fun addFragment(DestinationFragment: Fragment, containerResourceID: Int, fragmentManager: FragmentManager,isAnimate:Boolean) {

            var fragmentTransaction = fragmentManager?.beginTransaction()
            if(isAnimate)
            {
                fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
            }
            fragmentTransaction?.add(containerResourceID, DestinationFragment, "fragments")
            fragmentTransaction.addToBackStack(DestinationFragment.javaClass.name)
            fragmentTransaction?.commit()
        }

        @JvmStatic
        fun replaceFragment(DestinationFragment: Fragment, containerResourceID: Int, fragmentManager: FragmentManager,isAnimate: Boolean) {
            var fragmentTransaction = fragmentManager?.beginTransaction()
            if(isAnimate)
            {
                fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
            }
            fragmentTransaction?.replace(containerResourceID, DestinationFragment, "fragments")
            fragmentTransaction.addToBackStack(DestinationFragment.javaClass.name)
            fragmentTransaction?.commit()
        }
    }


//    override fun onInternetConnectivityChanged(isConnected: Boolean) {
//
//        if(isConnected)
//        {
//        Toast.makeText(activity,"Internet Connected Fragment", Toast.LENGTH_SHORT).show()
//            // SHOW DIALOG
//       //     hidedialog()
//        }
//        else
//        {
//          Toast.makeText(activity,"Internet Not Connected Fragment", Toast.LENGTH_SHORT).show()
//            // HIDE DIALOG
//         //   showdialog()
//        }
//
//    }

    private fun hidedialog() {

        if(alertDialog!=null)
        {
            alertDialog!!.dismiss()
        }

    }

    private fun showdialog()
    {
//        val builder = AlertDialog.Builder(requireActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen)
//        builder.setCancelable(false)
//        // set the custom layout
//        // set the custom layout
//        val customLayout: View = layoutInflater.inflate(R.layout.dialog_no_internet, null)
//        builder.setView(customLayout)
//         alertDialog = builder.create()
//        alertDialog!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
//        val width = ViewGroup.LayoutParams.MATCH_PARENT
//        val height = ViewGroup.LayoutParams.MATCH_PARENT
//        alertDialog!!.getWindow()!!.setLayout(width, height)
//        alertDialog!!.show()
//
//
//
//        val btn_retry = alertDialog!!.findViewById<View>(R.id.btn_retry) as AppCompatButton?
//
//        btn_retry!!.setOnClickListener { alertDialog!!.dismiss() }


        val builder = AlertDialog.Builder(requireActivity())
        builder.setCancelable(false)
        // set the custom layout
        // set the custom layout
        val customLayout: View = layoutInflater.inflate(R.layout.dialog_no_internet, null)
        builder.setView(customLayout)

        alertDialog = builder.create()
        alertDialog!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        alertDialog!!.show()
    }
}