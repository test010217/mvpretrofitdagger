package com.example.mvpretrofitdagger


import android.app.Application
import android.content.Context
import android.util.Log
import com.droidnet.DroidNet
import com.example.mvpretrofitdagger.dagger.component.DaggerNetComponent
import com.example.mvpretrofitdagger.dagger.component.NetComponent
import com.example.mvpretrofitdagger.dagger.module.APIModule
import com.example.mvpretrofitdagger.dagger.module.AppModule
import com.example.mvpretrofitdagger.dagger.module.NetModule
import com.example.mvpretrofitdagger.rest.ApiClient


class SBApp : Application(){

    companion object {

        lateinit var instance: SBApp
        private var sIsActivityOpen = false
        private var sIsChatActivityOpen = false
        fun isActivityOpen(): Boolean {
            return sIsActivityOpen
        }

        fun isChatActivityOpen():Boolean {
            return sIsChatActivityOpen
        }

        fun setActivityOpen(sIsActivityOpen: Boolean) {
            this.sIsActivityOpen = sIsActivityOpen
        }

        fun setChatActivityOpen(sIsChatActivityOpen: Boolean) {
            this.sIsChatActivityOpen = sIsChatActivityOpen
        }

    }

    lateinit var mNetComponent: NetComponent


    fun getmNetComponent(): NetComponent {
        return mNetComponent
    }

    override fun onCreate() {
        super.onCreate()
        init()
        instance = this
        DroidNet.init(this);

    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)

    }

    override fun onLowMemory() {
        super.onLowMemory()
        DroidNet.getInstance().removeAllInternetConnectivityChangeListeners();
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
    }

    override fun onTerminate() {
        super.onTerminate()
    }


    private fun init() {
        mNetComponent = DaggerNetComponent.builder()
            .appModule(AppModule(this))
            .netModule(NetModule(ApiClient.BASE_URL_APP))
            .aPIModule(APIModule())
            .build()
    }

}
