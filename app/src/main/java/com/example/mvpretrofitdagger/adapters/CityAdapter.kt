package com.example.mvpretrofitdagger.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mvpretrofitdagger.R
import com.example.mvpretrofitdagger.`interface`.onItemClick
import com.example.mvpretrofitdagger.adapters.CityAdapter.viewHolder
import com.example.mvpretrofitdagger.model.City

class CityAdapter(var context: Context, var list: MutableList<City>, var onItemClick: onItemClick) : RecyclerView.Adapter<viewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_city, parent, false)
        return viewHolder(view)
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val data = list!![position]
        val images = data.background
        Glide.with(context)
                .load(images)
                .placeholder(R.drawable.placeholder)
                .into(holder.iv_image)
        holder.tv_name.text = data.name

        holder.rl_layout.setOnClickListener {
            onItemClick.onClick(position)
        }

    }

    override fun getItemCount(): Int {
        return if (list != null) list!!.size else 0
    }

    inner class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var iv_image: ImageView
        var tv_name: TextView
        var rl_layout: RelativeLayout

        init {
            iv_image = itemView.findViewById<View>(R.id.iv_image) as ImageView
            tv_name = itemView.findViewById<View>(R.id.tv_name) as TextView
            rl_layout = itemView.findViewById<View>(R.id.rl_layout) as RelativeLayout

        }
    }

}