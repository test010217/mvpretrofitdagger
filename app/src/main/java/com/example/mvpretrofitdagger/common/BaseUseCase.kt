package com.example.mvpretrofitdagger.common

import android.content.Context

interface BaseUseCase {
    fun showLoader()

    fun hideLoader()

    fun getContext(): Context

    fun onBackPress()

    
}