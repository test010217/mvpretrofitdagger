package com.example.mvpretrofitdagger.common

import android.content.Context
import android.content.SharedPreferences
import com.example.mvpretrofitdagger.common.BaseUseCase
import com.example.mvpretrofitdagger.common.BaseView
import com.example.mvpretrofitdagger.common.CommonUtils
import com.example.mvpretrofitdagger.common.SharedPref
import com.example.mvpretrofitdagger.rest.SBAppInterface
import com.example.mvpretrofitdagger.SBApp
import com.google.gson.Gson
import javax.inject.Inject

abstract class BaseUseCaseImpl(var mBaseView: BaseView) : BaseUseCase {
//    lateinit var mBaseView: BaseView
    @Inject
    lateinit var sharedPreferences: SharedPreferences
    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var commonUtils: CommonUtils
    @Inject
    lateinit  var sbAppInterface: SBAppInterface
    @Inject
    lateinit var sharePref: SharedPref

//    fun BaseUseCaseImpl(baseView: BaseView) {
//        this.mBaseView = baseView
//    }

    init {
        val injector = SBApp.instance.getmNetComponent()
        injector.inject(this)
    }

   override fun getContext():Context {
        return mBaseView.getContextAppp()
    }


    override  fun showLoader() {
        mBaseView.showLoader()
    }

    override  fun hideLoader() {
        mBaseView.hideLoader()
    }

    override  fun onBackPress() {
        mBaseView.onBackPress()
    }
}
