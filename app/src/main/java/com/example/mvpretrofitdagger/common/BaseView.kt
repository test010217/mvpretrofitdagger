package com.example.mvpretrofitdagger.common

import android.content.Context

interface BaseView {


     fun showLoader()

     fun hideLoader()

     fun showErrorToast(msg: String)

     fun getContextAppp(): Context

     fun onBackPress()
}
