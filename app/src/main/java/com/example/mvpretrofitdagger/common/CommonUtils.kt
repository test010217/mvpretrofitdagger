package com.example.mvpretrofitdagger.common


import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.app.Dialog
import android.app.ProgressDialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.text.Html
import android.text.TextUtils
import android.text.format.DateFormat
import android.text.format.DateUtils
import android.transition.ChangeBounds
import android.transition.TransitionManager
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mvpretrofitdagger.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class CommonUtils(internal var mContext: Context) {


    val isNetworkAvailable: Boolean
        @SuppressLint("MissingPermission")
        get() {
            val cm = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            if (activeNetwork != null) {
                if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                    return true
                } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                    return true
                }
            } else {
                return false
            }
            return false
        }

    fun show1HoursAgoTime(dateTime: Date): Date {
        val cal = Calendar.getInstance()
        cal.time = dateTime
        cal.add(Calendar.HOUR_OF_DAY, 1)
        return cal.time
    }


    val isAboveOrEqualLolipop: Boolean
        get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP

    val isAboveOrEqualAndroid6: Boolean
        get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M

    val deviceID: String
        get() = Settings.Secure.getString(
            mContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )


    fun toastShow(message: String) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()
    }

    fun isLocationEnabled(context: Context): Boolean {
        var locationMode = 0
        val locationProviders: String

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode =
                    Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE)

            } catch (e: Settings.SettingNotFoundException) {
                e.printStackTrace()
                return false
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF

        } else {
            locationProviders = Settings.Secure.getString(
                context.contentResolver,
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED
            )
            return !TextUtils.isEmpty(locationProviders)
        }


    }


    // is service running ? check here
    fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = mContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    // soft keyboard hide
    fun hideKeyboard(activity: Activity) {
        // Check if no view has focus:
        try {
            val view = activity.currentFocus
            if (view != null) {
                val inputManager =
                    mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(
                    view.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            }
        } catch (e: Exception) {

        }

    }

    // soft keyboard hide
    fun hideKeyboard(activity: Activity, view: View?) {
        // Check if no view has focus:
        try {
            if (view != null) {
                val inputManager =
                    mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(view.windowToken, 0)
            }
        } catch (e: Exception) {

        }

    }

    // soft keyboard show
    fun showKeyboard(activity: Activity, view: View?) {
        // Check if no view has focus:
        try {
            if (view != null) {
                val imm =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
            }
        } catch (e: Exception) {

        }

    }

    //create dialog
    fun createDialog(): ProgressDialog {
        val progress = ProgressDialog(mContext, ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT)
        progress.setMessage(
            Html.fromHtml(
                "<font color='black'  ><big>"
                        + "Loading..." + "</big></font>"
            )
        )
        progress.setCancelable(false)
        return progress
    }

    fun showDialog(progressDialog: ProgressDialog?) {
        if (progressDialog != null) {
            if (!progressDialog.isShowing)
                progressDialog.show()
        }
    }

    fun dismissDialog(progressDialog: ProgressDialog?) {
        if (progressDialog != null) {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
        }
    }


    //check GPS or location is on or not
    fun canGetLocation(): Boolean {
        var result = true
        var lm: LocationManager? = null
        var gps_enabled = false
        var network_enabled = false
        lm = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        // exceptions will be thrown if provider is not permitted.
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (ex: Exception) {

        }

        try {
            network_enabled = lm
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (ex: Exception) {
        }

        if (gps_enabled == false || network_enabled == false) {
            result = false
        } else {
            result = true
        }

        return result
    }

    fun createCustomLoader(mContext: Context, isCancelable: Boolean): Dialog {
        val dialog = Dialog(mContext)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(isCancelable)
        dialog.setContentView(R.layout.loader_progress_dialog)


        //Grab the window of the dialog, and change the width
        val lp = WindowManager.LayoutParams()
        val window = dialog.window
        lp.copyFrom(window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp
        return dialog
    }

    fun showCustomDialog(dialog: Dialog?, context: Context) {
        if (dialog != null) {
            if (!dialog.isShowing)
                if (!(context as Activity).isFinishing) {
                    dialog.show()
                }
        }
    }

    fun dismissCustomDialog(dialog: Dialog?) {
        if (dialog != null) {
            if (dialog.isShowing)
                dialog.dismiss()
        }
    }


    //auto start permission page for only device that app's not working into background
    fun autostartPermission(context: Context) {
        try {
            val intent = Intent()
            val manufacturer = android.os.Build.MANUFACTURER
            Log.e("autostartPermission", "autostartPermission: $manufacturer")
            if ("xiaomi".equals(manufacturer, ignoreCase = true)) {
                intent.component =
                    ComponentName(
                        "com.miui.securitycenter",
                        "com.miui.permcenter.autostart.AutoStartManagementActivity"
                    )
                context.startActivity(intent)
            } else if ("huawei".equals(manufacturer, ignoreCase = true)) {
                intent.component =
                    ComponentName(
                        "com.huawei.systemmanager",
                        "com.huawei.systemmanager.optimize.process.ProtectActivity"
                    )
                context.startActivity(intent)
            } else if ("oppo".equals(manufacturer, ignoreCase = true)) {
//                context.startActivity(
//                    Intent().setComponent(
//                        ComponentName(
//                            "com.coloros.safecenter",
//                            "com.coloros.privacypermissionsentry.PermissionTopActivity"
//                        )
//                    )
//                )
                var intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                var uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                context.startActivity(intent);

//                intent.component =
//                    ComponentName(
//                        "com.coloros.safecenter",
//                        "com.coloros.safecenter.permission.startup.StartupAppListActivity"
//                    )
//                context.startActivity(intent)
            } else if ("vivo".equals(manufacturer, ignoreCase = true)) {
                intent.component =
                    ComponentName(
                        "com.vivo.permissionmanager",
                        "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"
                    )
                context.startActivity(intent)
            }


        } catch (e: Exception) {
            Log.e("autostartPermission", "autostartPermission: " + e.message)

        }

    }

}

