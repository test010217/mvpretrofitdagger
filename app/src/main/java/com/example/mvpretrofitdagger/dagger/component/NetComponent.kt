package com.example.mvpretrofitdagger.dagger.component

import com.example.mvpretrofitdagger.BaseFragment
import com.example.mvpretrofitdagger.BaseActivity
import com.example.mvpretrofitdagger.common.BaseUseCaseImpl
import com.example.mvpretrofitdagger.dagger.module.APIModule
import com.example.mvpretrofitdagger.dagger.module.AppModule
import com.example.mvpretrofitdagger.dagger.module.NetModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(NetModule::class, APIModule::class, AppModule::class))
interface NetComponent {
    fun inject(baseActivity: BaseActivity)
    fun inject(abstractFragment: BaseFragment)
    fun inject(baseUseCaseImpl: BaseUseCaseImpl)
}