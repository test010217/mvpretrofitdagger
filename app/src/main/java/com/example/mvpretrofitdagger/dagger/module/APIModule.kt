package com.example.mvpretrofitdagger.dagger.module


import com.example.mvpretrofitdagger.rest.SBAppInterface

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton


@Module(includes = arrayOf(NetModule::class))
class APIModule {
    @Provides
    @Singleton
    fun provideSbAppInterface(retrofit: Retrofit): SBAppInterface {
        return retrofit.create(SBAppInterface::class.java)
    }

}