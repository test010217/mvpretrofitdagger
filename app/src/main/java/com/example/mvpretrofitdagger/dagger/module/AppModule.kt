package com.example.mvpretrofitdagger.dagger.module

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(application:Application) {
    internal var mApplication: Application
    init{
        mApplication = application
    }
    @Provides
    @Singleton
    internal fun provideApplication():Application {
        return mApplication
    }
}