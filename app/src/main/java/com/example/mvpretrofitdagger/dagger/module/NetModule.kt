package com.example.mvpretrofitdagger.dagger.module

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.example.mvpretrofitdagger.common.CommonUtils
import com.example.mvpretrofitdagger.common.SharedPref
import com.example.mvpretrofitdagger.dagger.module.AppModule
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module(includes = arrayOf(AppModule::class))
class NetModule(mBaseUrl:String) {
    internal var mBaseUrl:String
    init{
        this.mBaseUrl = mBaseUrl
    }


    @Provides
    @Singleton
    internal fun provideSharedPreferences(application:Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application)
    }
    @Provides
    @Singleton
    internal fun provideOkHttpCache(application:Application):Cache {

        val cacheSize = 10 * 1024 * 1024 // 10 MiB
        val cache = Cache(application.getCacheDir(), cacheSize.toLong())
        return cache
    }
    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder.create()
    }
    @Provides
    @Singleton
    fun provideOkHttpClient(cache: Cache): OkHttpClient {
        val okClient = OkHttpClient.Builder()
        okClient.connectTimeout(30000, TimeUnit.MILLISECONDS)
        okClient.writeTimeout(30000, TimeUnit.MILLISECONDS)
        okClient.readTimeout(30000, TimeUnit.MILLISECONDS)

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        okClient.interceptors().add(interceptor)

        okClient.interceptors().add(Interceptor { chain ->
            val response = chain.proceed(chain.request())
            response.newBuilder()
                    .header("Cache-Control", "only-if-cached")

                    .build()
            response
        })
        return okClient.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(mBaseUrl)
                .client(okHttpClient)
                .build()
    }


    @Provides
    @Singleton
    internal fun provideCommonUtils(application: Application): CommonUtils {
        return CommonUtils(application)
    }
//    @Provides
//    @Singleton
//    internal fun provideValidation(application:Application): Validation {
//        return Validation(application)
//    }
    @Provides
    @Singleton
    internal fun providePref(application:Application): SharedPref {
        return SharedPref(application)
    }

}