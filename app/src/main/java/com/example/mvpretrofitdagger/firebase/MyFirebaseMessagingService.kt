package com.example.mvpretrofitdagger.firebase

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.mvpretrofitdagger.R
import com.example.mvpretrofitdagger.common.Conts
import com.example.mvpretrofitdagger.common.SharedPref
import com.example.mvpretrofitdagger.ui.mainactivity.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {

      var sharedPref:SharedPref?=null

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d("Firebasemessaging", "notif-->" + remoteMessage.data)
        Log.d("Firebasemessaging", "notif-->" + remoteMessage.notification)
        val name = "transaction_channel"
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        val pendingIntent = PendingIntent.getActivity(
                this, 0, intent,
                PendingIntent.FLAG_CANCEL_CURRENT
        )
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_HIGH
            )
            channel.description = CHANNEL_DESCRIPTION
            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(channel)
            val mBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle(remoteMessage.notification!!.title)
                    .setContentText(remoteMessage.notification!!.body)
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setSound(defaultSoundUri)
                    .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
                    .setLights(-0x1, 3000, 3000)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT) //                    .setContent(mContentView)
                    .setAutoCancel(true)
            val mNotificationManager = NotificationManagerCompat.from(this)
            mNotificationManager.notify(NOTIF_ID, mBuilder.build())
        } else {
            val mBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle(remoteMessage.notification!!.title)
                    .setContentText(remoteMessage.notification!!.body)
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setSound(defaultSoundUri)
                    .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
                    .setLights(-0x1, 3000, 3000)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT) //                    .setContent(mContentView)
                    .setAutoCancel(true)
            val mNotificationManager = NotificationManagerCompat.from(this)
            mNotificationManager.notify(NOTIF_ID, mBuilder.build())
        }
    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        println("token-->$s")
        sharedPref= SharedPref(this)
        sharedPref!!.setDataInPref(Conts.device_token,s)
    }

    companion object {
        private const val NOTIF_ID = 1
        const val CHANNEL_ID = "#123"
        const val CHANNEL_NAME = "my notification"
        const val CHANNEL_DESCRIPTION = "Test"
    }
}