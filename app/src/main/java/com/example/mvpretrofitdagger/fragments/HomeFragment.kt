package com.example.mvpretrofitdagger.fragments

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.droidnet.DroidListener
import com.example.mvpretrofitdagger.BaseFragment
import com.example.mvpretrofitdagger.R
import com.example.mvpretrofitdagger.`interface`.onItemClick
import com.example.mvpretrofitdagger.adapters.CityAdapter
import com.example.mvpretrofitdagger.common.Conts
import com.example.mvpretrofitdagger.model.City
import com.example.mvpretrofitdagger.model.ModelCitiesList
import com.example.mvpretrofitdagger.roomdb.AppDatabase
import com.example.mvpretrofitdagger.roomdb.AppExecutors
import com.example.mvpretrofitdagger.ui.home.HomeActivity
import com.example.mvpretrofitdagger.ui.mainactivity.MainActivityPresenter
import com.example.mvpretrofitdagger.ui.mainactivity.MainActivityPresenterImpl
import com.example.mvpretrofitdagger.ui.mainactivity.MainActivityView
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.DexterError
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.PermissionRequestErrorListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : BaseFragment(),MainActivityView,DroidListener
{
    lateinit var dialog: Dialog
    var cityAdapter: CityAdapter? = null
    var citiesList: MutableList<City> = ArrayList()
    lateinit var mainactivitypresenterimpl: MainActivityPresenter
    private var mDb: AppDatabase? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDb = AppDatabase.getInstance(requireActivity().applicationContext)
        dialog = commonUtils.createCustomLoader(requireActivity(), false)

        var linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = linearLayoutManager

        println("token=----->" + sharedPref.getDataFromPref(Conts.device_token))

        mainactivitypresenterimpl = MainActivityPresenterImpl(this)
        mainactivitypresenterimpl.getCities()
         checkPermissions()

    }


    private fun checkPermissions():Boolean {

        var permissiongranted=false;
        Dexter.withActivity(activity)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // all granted
                            permissiongranted=true
                            // showErrorToast("All permissions granted")
                            // code to execute task
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied) {
                            //     showErrorToast("Permission is denied permenantly, navigate user to app settings")
                            showSettingsDialog();
                            permissiongranted=false
                            // permission is denied permenantly, navigate user to app settings
                        }


                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest?>?, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                })
                .withErrorListener(object : PermissionRequestErrorListener
                {
                    override fun onError(error: DexterError?) {
                        permissiongranted=false
                        //  Toast.makeText(getApplicationContext(), "Error occurred! " + error.toString(), Toast.LENGTH_SHORT).show()
                    }

                })
                .onSameThread()
                .check()

        return permissiongranted
    }


    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private fun showSettingsDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS", DialogInterface.OnClickListener { dialog, which ->
            dialog.cancel()
            openSettings()
        })
        builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which ->

            dialog.cancel() })
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", requireActivity().packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }



    override fun getCitiesResponse(response: ModelCitiesList) {

        citiesList.clear()
     //   citiesList.addAll(response.data!!)
        AppExecutors.instance!!.diskIO().execute(Runnable {
             //   mDb!!.personDao()!!.insertPerson(citiesList)

            citiesList = mDb!!.cityDao()!!.loadAllCities() as MutableList<City>
            var templist:MutableList<City> =ArrayList()
            if(citiesList.size==0)
            {
                for (i in response.data!!.indices) {
                   // var fcity = City(response.data!!.get(i).name.toString(), response.data!!.get(i).description.toString(), response.data!!.get(i).background.toString())
                response.data!!.get(i).rid= Random().nextInt()
                    templist.add(response.data!!.get(i))
                }
                println("array size-->"+templist.size)
                mDb!!.cityDao()!!.insertCity(templist)
            }

        })
        retrieveTasks()
       // cityAdapter!!.notifyDataSetChanged()

    }

    override fun showLoader() {
        dialog.show()
    }

    override fun hideLoader() {

        if (dialog.isShowing) {
            dialog.dismiss()
        }
    }

    override fun showErrorToast(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun getContextAppp(): Context {
        return requireActivity()
    }

    override fun onBackPress() {
        //onBackPressed()
    }

    private fun retrieveTasks() {
        AppExecutors.instance!!.diskIO().execute(Runnable {
           citiesList = mDb!!.cityDao()!!.loadAllCities() as MutableList<City>
            println("list size-->"+citiesList.size)
            requireActivity().runOnUiThread(Runnable {
                cityAdapter = CityAdapter(requireActivity(), citiesList, object : onItemClick {
                    override fun onClick(position: Int) {

                        if(checkPermissions())
                        {
                            var intent= Intent(activity, HomeActivity::class.java)
                            intent.putExtra("id",citiesList.get(position).id)
                            startActivity(intent)
                            activity!!.overridePendingTransition(0,0)
                        }
                        else
                        {
                            checkPermissions()
                        }
                    }
                })
                recyclerView.adapter = cityAdapter

                cityAdapter!!.notifyDataSetChanged()
            })
        })
    }


    override fun onInternetConnectivityChanged(isConnected: Boolean) {

        Toast.makeText(activity,"i am called in fragment",Toast.LENGTH_SHORT).show()
    }

}