package com.example.mvpretrofitdagger.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.mvpretrofitdagger.BaseFragment
import com.example.mvpretrofitdagger.R
import com.example.mvpretrofitdagger.adapters.CityAdapter
import com.example.mvpretrofitdagger.model.City
import com.example.mvpretrofitdagger.roomdb.AppDatabase
import com.example.mvpretrofitdagger.roomdb.AppExecutors
import com.example.mvpretrofitdagger.ui.mainactivity.MainActivityPresenter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.my_fragment.*
import kotlinx.android.synthetic.main.my_fragment1.*


class MyFragment : BaseFragment()
{
    private var mDb: AppDatabase? = null
    var cid:Int=0
    var citi:City?=null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.my_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        var bundle=this.arguments
        if(bundle!=null)
        {
            cid=bundle.getInt("id")
        }
        mDb = AppDatabase.getInstance(requireActivity().applicationContext)
        AppExecutors.instance!!.diskIO().execute(Runnable {
             citi = mDb!!.cityDao()!!.loadCityById(cid)

            requireActivity()!!.runOnUiThread {
                Glide.with(requireActivity())
                        .load(citi!!.background)
                        .placeholder(R.drawable.placeholder)
                        .into(iv_image1)

                requireActivity().toolbar.setTitle(citi!!.name)
            }
        })

        iv_image1.setOnClickListener {
            println("on click my frag")

            var mfragment1=MyFragment1()
            val b = Bundle()
            b.putInt("id", cid)
            mfragment1.arguments = b
            replaceFragment(mfragment1,R.id.fragmentContainer1,requireActivity().supportFragmentManager,true)

        }


    }
}