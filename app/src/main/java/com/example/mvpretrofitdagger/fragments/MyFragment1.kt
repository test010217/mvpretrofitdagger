package com.example.mvpretrofitdagger.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.mvpretrofitdagger.BaseFragment
import com.example.mvpretrofitdagger.R
import com.example.mvpretrofitdagger.model.City
import com.example.mvpretrofitdagger.roomdb.AppDatabase
import com.example.mvpretrofitdagger.roomdb.AppExecutors
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.my_fragment.*
import kotlinx.android.synthetic.main.my_fragment1.*

 class MyFragment1 : BaseFragment()
{
    private var mDb: AppDatabase? = null
    var cid:Int=0
    var citi: City?=null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.my_fragment1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().toolbar.setTitle("My Fragment1")

        var bundle=this.arguments
        if(bundle!=null)
        {
            cid=bundle.getInt("id")
            println("cid-->"+cid)
        }
        mDb = AppDatabase.getInstance(requireActivity().applicationContext)
        AppExecutors.instance!!.diskIO().execute(Runnable {
            citi = mDb!!.cityDao()!!.loadCityById(cid)
            requireActivity()!!.runOnUiThread {
                Glide.with(requireActivity())
                        .load(citi!!.background)
                        .placeholder(R.drawable.placeholder)
                        .into(iv_image2)
            }
        })

        iv_image2.setOnClickListener {
            println("on click my frag1")
            replaceFragment(MyFragment2(),R.id.fragmentContainer1,requireActivity().supportFragmentManager,true)



        }
    }
}