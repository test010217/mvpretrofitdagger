package com.example.mvpretrofitdagger.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.mvpretrofitdagger.BaseFragment
import com.example.mvpretrofitdagger.R
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.my_fragment.*

 class MyFragment2 : BaseFragment()
{

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.my_fragment2, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().toolbar.setTitle("User Fragment")
    }
}