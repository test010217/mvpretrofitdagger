package com.example.mvpretrofitdagger.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "city")
class City {

    @SerializedName("id")
    @Expose
    var id: Int?=null


    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("background")
    @Expose
    var background: String? = null


    @PrimaryKey(autoGenerate = true)
    var rid:Int =0

    @Ignore
    constructor(name: String, description: String, background: String,rid:Int) {
        this.name = name
        this.description = description
        this.background = background
        this.rid=rid
    }

    constructor(id: Int, name: String, description: String, background: String,rid:Int) {
        this.id = id
        this.name = name
        this.description = description
        this.background = background
        this.rid=rid
    }

    constructor()

}