package com.example.mvpretrofitdagger.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class ModelCitiesList {
    @SerializedName("data")
    @Expose
    var data: ArrayList<City>? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("status")
    @Expose
    var status = 0

}