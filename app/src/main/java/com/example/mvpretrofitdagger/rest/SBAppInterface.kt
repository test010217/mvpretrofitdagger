package com.example.mvpretrofitdagger.rest

import com.example.mvpretrofitdagger.model.ModelCitiesList
import retrofit2.Call
import retrofit2.http.GET


interface SBAppInterface {

    @GET("v1/city")
    fun getCitiesList(): Call<ModelCitiesList>
}