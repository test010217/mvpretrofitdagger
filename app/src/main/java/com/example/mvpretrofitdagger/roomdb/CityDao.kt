package com.example.mvpretrofitdagger.roomdb

import androidx.room.*
import com.example.mvpretrofitdagger.model.City

@Dao
interface CityDao {
    @Query("SELECT * FROM CITY")
    fun loadAllCities(): List<City?>?

    @Insert
    fun insertCity(city: MutableList<City>)

    @Update
    fun updateCity(city: City?)

    @Delete
    fun delete(city: MutableList<City>)

    @Query("SELECT * FROM CITY WHERE id = :id")
    fun loadCityById(id: Int): City?
}