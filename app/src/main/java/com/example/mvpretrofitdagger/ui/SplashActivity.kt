package com.example.mvpretrofitdagger.ui

import android.content.Intent
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.mvpretrofitdagger.R
import com.example.mvpretrofitdagger.ui.login.LoginActivity
import com.example.mvpretrofitdagger.ui.mainactivity.MainActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val iv: ImageView = findViewById(R.id.image)
        val tv: TextView = findViewById(R.id.welcomeText)
        val animation: Animation = AnimationUtils.loadAnimation(this, R.anim.splashtransition)
        iv.startAnimation(animation)
        tv.startAnimation(animation)
        val i = Intent(this, LoginActivity::class.java)
        val timer: Thread = object : Thread() {
            override fun run() {
                try {
                    sleep(3000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } finally {
                    startActivity(i)
                    finish()
                }
            }
        }
        timer.start()
    }
}