package com.example.mvpretrofitdagger.ui.home

import android.os.Bundle
import com.example.mvpretrofitdagger.BaseActivity
import com.example.mvpretrofitdagger.BaseFragment
import com.example.mvpretrofitdagger.R
import com.example.mvpretrofitdagger.fragments.MyFragment
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity: BaseActivity()
{
    var id=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        var bundle=intent.extras
        if(bundle!=null)
        {
            id=bundle.getInt("id")
        }

        toolbar.setTitle("Detail")
        toolbar.setTitleTextColor(resources.getColor(R.color.colorWhite))

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)

        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }


        var mfragment=MyFragment()
        val b = Bundle()
        b.putInt("id", id)
        mfragment.arguments = b
        BaseFragment.replaceFragment(mfragment, R.id.fragmentContainer1,supportFragmentManager,false)

    }

    override fun onBackPressed() {
      super.onBackPressed()
    overridePendingTransition(0,0)
        val count = supportFragmentManager.backStackEntryCount
        if (count >0) {
            supportFragmentManager.popBackStack(supportFragmentManager.findFragmentByTag("fragments").toString(),0)
        }
        else
        {
            super.onBackPressed()
          overridePendingTransition(0,0)
        }
    }
}