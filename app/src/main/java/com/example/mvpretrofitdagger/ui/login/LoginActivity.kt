package com.example.mvpretrofitdagger.ui.login

import android.content.Intent
import android.os.Bundle
import com.example.mvpretrofitdagger.BaseActivity
import com.example.mvpretrofitdagger.R
import com.example.mvpretrofitdagger.ui.verifyotp.VerifytOtpActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity :BaseActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_login.setOnClickListener {
            var intent= Intent(this@LoginActivity,VerifytOtpActivity::class.java)
            startActivity(intent)
        }
    }
}