package com.example.mvpretrofitdagger.ui.mainactivity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.example.mvpretrofitdagger.BaseActivity
import com.example.mvpretrofitdagger.BaseFragment
import com.example.mvpretrofitdagger.R
import com.example.mvpretrofitdagger.fragments.HomeFragment
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initview()

    }

    private fun initview() {

        setSupportActionBar(toolbar)

        val drawerToggle = ActionBarDrawerToggle(this, drawer, R.string.open, R.string.close)
        drawer.addDrawerListener(drawerToggle)
        drawerToggle.syncState()

        navigation_view.setNavigationItemSelectedListener(this);

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        BaseFragment.addFragment(HomeFragment(),R.id.fragmentContainer,supportFragmentManager,false)
    }


    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            finish()
            super.onBackPressed()

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                drawer.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

 //       when(item.itemId)
//        {
//            R.id.nav_home ->
//            {
//                BaseFragment.replaceFragment(HomeFragment(),R.id.fragmentContainer,supportFragmentManager)
//            }
//
//            R.id.nav_gallery ->
//            {
//
//            }
//
//            R.id.nav_slideshow ->
//            {
//
//            }
//
//            R.id.nav_tools ->
//            {
//
//            }
//
//
//            R.id.nav_share ->
//            {
//
//            }
//
//            R.id.nav_send ->
//            {
//
//            }
//
//
//            R.id.nav_logout ->
//            {
//
//            }

    //    }
        BaseFragment.replaceFragment(HomeFragment(),R.id.fragmentContainer,supportFragmentManager,false)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }




}
