package com.example.mvpretrofitdagger.ui.mainactivity

// for passing parameters for api
interface MainActivityPresenter {

    fun getCities()

}
