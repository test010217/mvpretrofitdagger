package com.example.mvpretrofitdagger.ui.mainactivity

import android.util.Log
import com.example.mvpretrofitdagger.common.BaseUseCaseImpl
import com.example.mvpretrofitdagger.rest.ApiClient
import com.example.mvpretrofitdagger.R
import com.example.mvpretrofitdagger.model.ModelCitiesList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivityPresenterImpl(internal var mainactivityView: MainActivityView) : BaseUseCaseImpl(mainactivityView),
    MainActivityPresenter {

    override fun getCities() {

        if (commonUtils.isNetworkAvailable) {
            mainactivityView.showLoader()
            val sbAppInterface = ApiClient.getSbAppInterface()
            val call = sbAppInterface.getCitiesList()

            call!!.enqueue(object : Callback<ModelCitiesList> {
                override fun onResponse(call: Call<ModelCitiesList>, response: Response<ModelCitiesList>) {
                    Log.d("onResponse", "success----------->>>>>>>>" + response.body())
                    //     loginView.hideLoader()
                    if (mainactivityView != null) {
                        mainactivityView.hideLoader()
                        if (response.isSuccessful) {
                            mainactivityView.getCitiesResponse(response.body()!!)
                        }
                    } else {
                        if (response.body()!!.message != null) {
                            mainactivityView.showErrorToast(response.body()!!.message.toString())
                        }
                        else {
                            mainactivityView.showErrorToast(getContext().resources.getString(R.string.something_went_wrong))
                        }
                    }
                }

                override fun onFailure(call: Call<ModelCitiesList>, t: Throwable) {
                    Log.d("onResponse", "onFailure---->>" + t.message)
                    mainactivityView.hideLoader()
                    mainactivityView.showErrorToast(getContext().resources.getString(R.string.something_went_wrong))
                }
            })

        } else {
            mainactivityView.showErrorToast(getContext().resources.getString(R.string.internet_connection_error))
        }
    }

}