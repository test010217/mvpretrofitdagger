package com.example.mvpretrofitdagger.ui.mainactivity

import com.example.mvpretrofitdagger.common.BaseView
import com.example.mvpretrofitdagger.model.ModelCitiesList


interface MainActivityView : BaseView {

    fun getCitiesResponse(response: ModelCitiesList)
}