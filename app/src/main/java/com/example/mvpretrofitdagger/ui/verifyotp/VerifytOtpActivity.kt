package com.example.mvpretrofitdagger.ui.verifyotp

import android.content.Intent
import android.os.Bundle
import com.example.mvpretrofitdagger.BaseActivity
import com.example.mvpretrofitdagger.R
import com.example.mvpretrofitdagger.ui.mainactivity.MainActivity
import kotlinx.android.synthetic.main.activity_verify_otp.*

class VerifytOtpActivity :BaseActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_otp)

        btn_send_otp.setOnClickListener {

            var intent= Intent(this@VerifytOtpActivity,MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }
}